
package com.alejandro.app.Disney.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="pelicula")

public class Pelicula implements Serializable{
    
    private static final long serialVersionUID = 8989711152006644060L;

	private static final String Date = null;
	
	public Pelicula() {}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idPelicula;
  
	@Column(length=50)
	private String titulo;
	
	@Column()
	private Date fechaCreacion;  
	
	@Column()
	private int calificacion;
	
	@Column()
	private int idPersonaje;

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public int getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(int calificacion) {
		this.calificacion = calificacion;
	}

	public int getIdPersonaje() {
		return idPersonaje;
	}

	public void setIdPersonaje(int idPersonaje) {
		this.idPersonaje = idPersonaje;
	}

	public static String getDate() {
		return Date;
	}
	
    
}
