
package com.alejandro.app.Disney.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="genero")
public class Genero implements Serializable{
   
    private static final long serialVersionUID = 3401914722393182304L;

	public Genero() {}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idGenero;
	@Column(length=50)
	private String nombre;
	
	@Column(name="idpelicula")
	private int idPelicula;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getIdPelicula() {
		return idPelicula;
	}

	public void setIdPelicula(int idPelicula) {
		this.idPelicula = idPelicula;
	}
	
    
    
}
