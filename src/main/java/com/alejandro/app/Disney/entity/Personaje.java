
package com.alejandro.app.Disney.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="personajes")

public class Personaje implements Serializable{
    
    private static final long serialVersionUID = 6994621035826183513L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
        @Column
	private int edad;
	
	@Column(length=50)
	private String nombre;
	
	private long peso;
	
	@Column(length=50)
	private String historia;
	
	
	public Long getId() {
		return id;
	}

    public void setId(long id) {
        this.id = id;
    }

	

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public long getPeso() {
		return peso;
	}

	public void setPeso(long peso) {
		this.peso = peso;
	}

	public String getHistoria() {
		return historia;
	}

	public void setHistoria(String historia) {
		this.historia = historia;
	}

	
	
	public Personaje() {}
	
	

    
}
