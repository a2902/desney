/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alejandro.app.Disney.service;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.alejandro.app.Disney.entity.Pelicula;

public interface PeliculaService {
        public Iterable<Pelicula>findAll();
	
	public Page<Pelicula>findAll(Pageable pageable);
	
	public Optional<Pelicula>findById(Long id);
	
	public Pelicula save(Pelicula pelicula);
	
	public void deleteById(Long id);

    
}
