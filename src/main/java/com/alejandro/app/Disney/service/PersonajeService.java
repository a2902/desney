
package com.alejandro.app.Disney.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


import com.alejandro.app.Disney.entity.Personaje;

public interface PersonajeService {
    
       public Iterable<Personaje>findAll();
	
	public Page<Personaje>findAll(Pageable pageable);
	
	public Optional<Personaje>findById(Long id);
	
	public Personaje save(Personaje personaje);
	
	public void deleteById(Long id);

}
