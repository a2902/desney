
package com.alejandro.app.Disney.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.alejandro.app.Disney.entity.Pelicula;
import com.alejandro.app.Disney.repository.PeliculaRepository;

@Service
public class PeliculaServiceImpl implements PeliculaService{
    
       @Autowired
	private PeliculaRepository peliculaRepository;

	@Override
	@Transactional(readOnly=true)
	public Iterable<Pelicula> findAll() {
		return peliculaRepository.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public Page<Pelicula> findAll(Pageable pageable) {
		return peliculaRepository.findAll(pageable);
	}

	@Override
	@Transactional(readOnly=true)
	public Optional<Pelicula> findById(Long id) {
		return peliculaRepository.findById(id);
	}

	@Override
	@Transactional
	public Pelicula save(Pelicula pelicula) {
		return peliculaRepository.save(pelicula);
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
		peliculaRepository.deleteById(id);		
	}
	

}
