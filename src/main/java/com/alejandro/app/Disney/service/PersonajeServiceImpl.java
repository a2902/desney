
package com.alejandro.app.Disney.service;

import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alejandro.app.Disney.entity.Personaje;
import com.alejandro.app.Disney.repository.PersonajeRepository;

@Service
public class PersonajeServiceImpl implements PersonajeService{
     
        @Autowired 
	private PersonajeRepository personajeRepository;

	@Override
	@Transactional(readOnly=true)
	public Iterable<Personaje> findAll() {
		return personajeRepository.findAll();
	}

	@Override
	@Transactional(readOnly=true)
	public Page<Personaje> findAll(Pageable pageable) {
	
		return personajeRepository.findAll(pageable);
	}

	@Override
	@Transactional(readOnly=true)
	public Optional<Personaje> findById(Long id) {
		return personajeRepository.findById(id);
	}

	@Override
	@Transactional(readOnly=true)
	public Personaje save(Personaje personaje) {
	
		return personajeRepository.save(personaje);
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
		personajeRepository.deleteById(id);
	}

}
