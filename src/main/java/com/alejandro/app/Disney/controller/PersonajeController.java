
package com.alejandro.app.Disney.controller;

import com.alejandro.app.Disney.entity.Personaje;
import com.alejandro.app.Disney.service.PersonajeService;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/app/dbspring")
public class PersonajeController implements Serializable{
    
    @Autowired
	private PersonajeService personajeService;
    
     //Create a new Personaje
	@PostMapping
	public ResponseEntity<?>createPersonaje(Personaje personaje){
		return ResponseEntity.status(HttpStatus.CREATED).body(personajeService.save(personaje));
	}
        //Read a Pelicula
	@GetMapping("/{id}")
	public ResponseEntity<?>readPersonaje(@PathVariable(value="id")Long personajeId){
		Optional<Personaje>oPelicula = personajeService.findById(personajeId);
		
		if(!oPelicula.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(oPelicula);
	}
        
         //Update a Personaje
	@PutMapping("/{id}")
	public ResponseEntity<?>updatePersonaje(@RequestBody Personaje PersonajeDetails, @PathVariable(value="id")Long peliculaId){
		Optional<Personaje>oPersonaje = personajeService.findById(peliculaId);
		if(!oPersonaje.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		oPersonaje.get().setNombre(PersonajeDetails.getNombre());
		oPersonaje.get().setEdad(PersonajeDetails.getEdad());
                oPersonaje.get().setHistoria(PersonajeDetails.getHistoria());
		oPersonaje.get().setPeso(PersonajeDetails.getPeso()); // me funciona.
                oPersonaje.get().setId(PersonajeDetails.getId());
		
         return ResponseEntity.status(HttpStatus.CREATED).body(personajeService.save(oPersonaje.get()));
		
		
	}
        //Delete a Personaje
        @DeleteMapping("/{id}")
        public ResponseEntity<?>deletePersonaje(@PathVariable(value="id")Long generoId){
            if(!personajeService.findById(generoId).isPresent()){
                return ResponseEntity.notFound().build();
            }
            personajeService.deleteById(generoId);
            return ResponseEntity.ok().build();
        }
        
        // Read all Personajes
        @GetMapping
        public List<Personaje>readAllPersonaje(){
            List<Personaje>generos =StreamSupport.stream(personajeService.findAll().spliterator(),false).
                    collect(Collectors.toList());
            return generos;
        }
    
}
