
package com.alejandro.app.Disney.controller;

import com.alejandro.app.Disney.entity.Pelicula;
import com.alejandro.app.Disney.service.PeliculaService;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/app/dbspring")
public class PeliculaController implements Serializable{
    
    @Autowired
	private PeliculaService peliculaService;
    
    //Create a new Pelicula
	@PostMapping("/crear")
	public ResponseEntity<?>createPelicula(Pelicula pelicula){
		return ResponseEntity.status(HttpStatus.CREATED).body(peliculaService.save(pelicula));
	}
        //Read a Pelicula
	@GetMapping("/leer")
	public ResponseEntity<?>readPelicula(@PathVariable(value="id")Long generoId){
		Optional<Pelicula>oPelicula = peliculaService.findById(generoId);
		
		if(!oPelicula.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(oPelicula);
	}
         //Update a Pelicula
	@PutMapping("/acualizar")
	public ResponseEntity<?>updatePelicula(@RequestBody Pelicula PeliculaDetails, @PathVariable(value="id")Long peliculaId){
		Optional<Pelicula>oPelicula = peliculaService.findById(peliculaId);
		if(!oPelicula.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		oPelicula.get().setTitulo(PeliculaDetails.getTitulo());
		oPelicula.get().setIdPersonaje(PeliculaDetails.getIdPersonaje());
                oPelicula.get().setCalificacion(PeliculaDetails.getCalificacion());
		oPelicula.get().setFechaCreacion(PeliculaDetails.getFechaCreacion()); // me funciona.
		
         return ResponseEntity.status(HttpStatus.CREATED).body(peliculaService.save(oPelicula.get()));
		
		
	}
        //Delete a Pelicula
        @DeleteMapping("/borrar")
        public ResponseEntity<?>deletePelicula(@PathVariable(value="id")Long generoId){
            if(!peliculaService.findById(generoId).isPresent()){
                return ResponseEntity.notFound().build();
            }
            peliculaService.deleteById(generoId);
            return ResponseEntity.ok().build();
        }
        
        // Read all Peliculas
        @GetMapping(value="/goodbye")
        public List<Pelicula>readAllPeliculas(){
            List<Pelicula>generos =StreamSupport.stream(peliculaService.findAll().spliterator(),false).
                    collect(Collectors.toList());
            return generos;
        }
    
}
