
package com.alejandro.app.Disney.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.alejandro.app.Disney.entity.Personaje;

@Repository
public interface PersonajeRepository extends JpaRepository<Personaje, Long> {
    
}
